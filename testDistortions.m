% Load possible distortions for the lightfields folder and the defined
% quality
dist=CreateDistortions('./lightfields/',0.5);

%%
len=dist.len();
ids={'angle_blur_2','angle_blur_4','angle_missing_1of5','angle_missing_1of10','angle_misalign_1','angle_misalign_2','lego_missing_1of5','lego_missing_1of10','lego_noise_1','lego_noise_2','lion_noise_1','lion_noise_2','lion_contrast_2','lion_contrast_3','cave_contrast_2','cave_contrast_3','cave_blur_2','cave_blur_4'};
%deal(dist.getIds());
results=(1:18)*0;
userRatings=normaliseData();
%%
for i=1:len
    id=ids(i);
    disp(strcat(num2str(i),'/',num2str(len), 'Working on: ',id(1)));
    ret=dist.getDistortedLF(id{1});
    [LF,dLF,userRating]=deal(ret{:});

    results(i) = customSSIM(LF,dLF);
end
%%
predictions=zeros(18,2);
predictions(:,1)=userRatings;
predictions(:,2)=results;
csvwrite('predictions.csv',predictions);
reviewData(userRatings,results);