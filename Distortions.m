classdef Distortions < handle
    properties (Constant)
      ID = 1;
      LOAD_OPTIONS = 2;
      DIST_FUNCTION = 3;
      RATING = 4;
      LF_PATH = 1;
      LF_U = 2;
      LF_FIRSTIMPOS = 3;
    end
    properties
        dist
        lfCache
        lfDir
        loadQuality
	end
    methods
        function obj = Distortions(lightFieldDirectory, loadQuality)
            obj.lfDir = lightFieldDirectory;
            obj.dist = containers.Map;
            obj.lfCache = containers.Map;
            obj.loadQuality = loadQuality;
        end
        function obj=add(obj,id,loadOptions,distortionFunction,rating)
            loadOptions{Distortions.LF_PATH} = strcat(obj.lfDir,loadOptions{Distortions.LF_PATH});
            obj.dist(id) = {id,loadOptions,distortionFunction,rating};
        end
        function ret = getDistortedLF(obj,id)
            d = obj.dist(id);
            [path,U,firstimpos]=deal(d{Distortions.LOAD_OPTIONS}{:});            
            if isKey(obj.lfCache,path)
                LF = obj.lfCache(path);
            else
                LF = ImportLF(path,U,firstimpos,obj.loadQuality);
                obj.lfCache(path)=LF;
            end
            
            distFun = d{Distortions.DIST_FUNCTION};
            %disp(func2str(distFun));
            dLF = distFun(LF);
            rating = d{Distortions.RATING};
            ret = {LF,dLF,rating};
        end
        function s = len(obj)
            s = size(obj.dist);
            s=s(1);
        end
        function ids = getIds(obj)
            ids=keys(obj.dist);
        end
        function save(obj,id,LF)
            d = obj.dist(id);
            [path,~,firstimpos]=deal(d{Distortions.LOAD_OPTIONS}{:});  
            pathTo=strcat(obj.lfDir,id,'/');
            SaveLF(path,pathTo,firstimpos,LF);
            zipPath=strcat(obj.lfDir,id,'.zip');
            PrepareForStanford(pathTo,firstimpos,zipPath);
        end
        function prepareOriginal(obj,id,name)
            d = obj.dist(id);
            [path,~,firstimpos]=deal(d{Distortions.LOAD_OPTIONS}{:});
            zipPath=strcat(obj.lfDir,name,'.zip');
            PrepareForStanford(path,firstimpos,zipPath);
        end
    end
end
