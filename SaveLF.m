function SaveLF(pathFrom,pathTo,firstimpos,LF)
% IMPORTLF Saves a lightfield to the defined location (pathTo) and uses the
% filenames from pathFrom
    disp('start saving light field');
    
    % list light field perspectives
    filesFrom = dir(strcat(pathFrom,'/*.png'));

    % compute number vertical light field perspectives.
    [T,S,c,U,V] = size(LF);

    if(~exist(pathTo,'dir'))
        mkdir(pathTo);
    end
    for i=1:size(filesFrom,1)
        % compute current u and v coordinate
        cv = floor((i-1)/U)+1;
        cu = mod((i-1),U)+1;
        
        % invert if first image is not top left image
        if(firstimpos(1) == 1)
            cu = U-cu+1;
        end
        if(firstimpos(2) == 1)
            cv = V-cv+1;    
        end
        
        % load image from file
        imagename = strcat(pathTo,'/',filesFrom(i).name);
        imwrite(LF(:,:,:,cu,cv),imagename);
    end
    
    disp('finished saving light field');
end

