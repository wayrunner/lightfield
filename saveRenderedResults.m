dist=CreateDistortions('./lightfields/',0.5);

disparities=[-1,-0.5,0,0.5,1];
apetures=[1,2,5,8];
us=[-4,0,4];
vs=us;
lfCnt=dist.len();
renderCount=length(disparities)*length(apetures)*length(us)*length(vs);
ids={'angle_blur_2','angle_blur_4','angle_missing_1of5','angle_missing_1of10','angle_misalign_1','angle_misalign_2','lego_missing_1of5','lego_missing_1of10','lego_noise_1','lego_noise_2','lion_noise_1','lion_noise_2','lion_contrast_2','lion_contrast_3','cave_contrast_2','cave_contrast_3','cave_blur_2','cave_blur_4'};
results=zeros(lfCnt,renderCount);
title='';
current=1;
for i=1:4
    id=ids(i);
    ret=dist.getDistortedLF(id{1});
    [LF1,LF2,userRating]=deal(ret{:});
    [S,T,c,U,V] = size(LF1);
    col=1;
    for di=disparities
        for ap=apetures
            for u=us
                for v=vs
                    if i==1
                        title=strcat(title,',d=',num2str(di),' a=',num2str(ap),' u=',num2str(u),' v=',num2str(v));
                    end

                    results(i,col)=CustomRenderSSIM(LF1,LF2,di,ap,u,v);
                    col=col+1;
                    disp(strcat(num2str(current),'/',num2str(renderCount*lfCnt/18*4)));
                    current=current+1;
                end
            end
        end
    end
end
disp(title);
csvwrite('renderResults2.csv',results);
