function LF = lfrgb2gray( RGB_LF )
%LFRGB2GRAY Convert RGB lightfield to grayscale
%   LFRGB2GRAY converts RGB lightfields to grayscale by eliminating 
%   the hue and saturation information while retaining the luminance.
%
%   LF = LFRGB2GRAY(RGB_LF) converts the RGB lightfield RGB_LF
%   to the grayscale intensity lightfield LF.

LF = squeeze(0.2989 * RGB_LF(:,:,1,:,:) + ...
                0.5870 * RGB_LF(:,:,2,:,:) + ...
                0.1140 * RGB_LF(:,:,3,:,:));

end

