function dist = CreateDistortions(lfPath,quality)
    dist = Distortions(lfPath,quality);
    amethyst = {'amethyst_512_17x17/',17,[1,1]};
    angle = {'tarot_smallangle_17x17/',17,[1,1]};
    lego = {'legoknights-small_17x17/',17,[1,1]};
    lion = {'sintel_lion_512_19x19/',19,[0,1]};
    cave = {'sintel_cave_entrance_512_19x19/',19,[0,1]};
    %beans = {'beans/',17,[0,1]};

    % The rating should be defined for each error and lightfield
    % individually through a user study
    rating = 0.1;
    
%     dist.add('angle',angle,@errorNone,rating);
%     dist.add('lion',lion,@errorNone,rating);
%     dist.add('cave',cave,@errorNone,rating);
%     dist.add('lego',lego,@errorNone,rating);


    dist.add('amethyst_missing_1of5',amethyst,@errorMissing_1of5,0.8089);
    dist.add('amethyst_contrast_2',amethyst,@errorContrast_2,0.6104);
    dist.add('amethyst_blur_2',amethyst,@errorBlur_2,0.6091);

    dist.add('amethyst_missing_blur',amethyst,@errorBlur_Missing,0.6091);
    dist.add('amethyst_noise_contrast',amethyst,@errorContrast_Noise,0.6104);

    dist.add('angle_blur_2',angle,@errorBlur_2,0.6091);
    dist.add('angle_blur_4',angle,@errorBlur_4,0.4956);
    dist.add('angle_missing_1of5',angle,@errorMissing_1of5,0.8089);
    dist.add('angle_missing_1of10',angle,@errorMissing_1of10,0.8530);
    dist.add('angle_misalign_1', angle, @errorMisalignment_1, 0.9314);
    dist.add('angle_misalign_2', angle, @errorMisalignment_2, 0.9171);
    dist.add('lego_missing_1of5',lego,@errorMissing_1of5,0.7774);
    dist.add('lego_missing_1of10',lego,@errorMissing_1of10,0.8338);
    dist.add('lego_noise_1',lego,@errorNoise_1,0.4511);
    dist.add('lego_noise_2',lego,@errorNoise_2,0.3059);
    dist.add('lion_noise_1',lion,@errorNoise_1,0.3121);
    dist.add('lion_noise_2',lion,@errorNoise_2,0.2422);
    dist.add('lion_contrast_2',lion,@errorContrast_2,0.6104);
    dist.add('lion_contrast_3',lion,@errorContrast_3,0.4223);
    dist.add('cave_contrast_2',cave,@errorContrast_2,0.7152);
    dist.add('cave_contrast_3',cave,@errorContrast_3,0.5787);
    dist.add('cave_blur_2',cave,@errorBlur_2,0.5240);
    dist.add('cave_blur_4',cave,@errorBlur_4,0.4092);
end

function distLF = errorNone(LF)
    distLF=LF;
end
% missing
function distLF = errorMissing(LF,modValue,partValue)
    [T,S,c,U,V] = size(LF);
    distLF=zeros(T,S,c,U,V,'uint8');
    i=0;
    for cu=1:U
        for cv=1:V
            if(mod(i,modValue)+1>partValue)
                im=LF(:,:,:,cu,cv);%im2double();%im2double(LF(:,:,:,cu,cv));
                distLF(:,:,:,cu,cv) = im;%imfilter(im, kernel, 'replicate'); % filter ones
            end
            i=i+1;
        end
    end
end
function distLF = errorMissing_1of2(LF)
    distLF=errorMissing(LF,2,1);
end
function distLF = errorMissing_1of10(LF)
    distLF=errorMissing(LF,10,1);
end
function distLF = errorMissing_1of5(LF)
    distLF=errorMissing(LF,5,1);
end

% blur
function distLF = errorBlur(LF,blur)
    [T,S,c,U,V] = size(LF);
    distLF=zeros(T,S,c,U,V,'uint8');
    kernel = fspecial('gaussian', 5*blur, 1*blur);
    for cu=1:U
        for cv=1:V
            im=LF(:,:,:,cu,cv);%im2double();%im2double(LF(:,:,:,cu,cv));
            distLF(:,:,:,cu,cv) = imfilter(im, kernel, 'replicate'); % filter ones
        end
    end
end

function distLF = errorBlur_1(LF)
    distLF = errorBlur(LF,1);
end

function distLF = errorBlur_2(LF)
    distLF = errorBlur(LF,2);
end


function distLF = errorBlur_4(LF)
    distLF = errorBlur(LF,4);
end

function distLF = errorBlur_Missing(LF)
    distLF = errorBlur(LF,4);
    distLF=errorMissing(distLF,5,1);
end

% noise
function distLF = errorNoise(LF,density)
    % slightly easier and more efficient...
    distLF = imnoise(LF, 'salt & pepper', density);
%    [T,S,c,U,V] = size(LF);
%    distLF=zeros(T,S,c,U,V,'uint8');
%    for cu=1:U
%        for cv=1:V
%            im=LF(:,:,:,cu,cv);%im2double();%im2double(LF(:,:,:,cu,cv));
%            distLF(:,:,:,cu,cv) = imnoise(im, 'salt & pepper',density); % filter ones
%        end
%    end
end

function distLF = errorNoise_1(LF)
    distLF = errorNoise(LF,0.11);
end

function distLF = errorNoise_2(LF)
    distLF = errorNoise(LF,0.22);
end
function distLF = errorNoise_3(LF)
    distLF = errorNoise(LF,0.33);
end

function distLF = errorNoise_4(LF)
    distLF = errorNoise(LF,0.44);
end
function distLF = errorNoise_5(LF)
    distLF = errorNoise(LF,0.55);
end
function distLF = errorNoise_8(LF)
    distLF = errorNoise(LF,0.88);
end


% contrast
function distLF = errorContrast_1(LF)
    rng(2)
    distLF = lfadjust(LF, 0.1);
end

function distLF = errorContrast_2(LF)
    rng(4)
    distLF = lfadjust(LF, 0.2);
end

function distLF = errorContrast_3(LF)
    rng(16)
    distLF = lfadjust(LF, 0.4);
end

function distLF = errorContrast_Noise(LF)
    rng(16)
    distLF = lfadjust(LF, 0.4);
    distLF = errorNoise(distLF,0.11);
end

% errorMisalignment
function distLF = copy(distLF, cu1, cv1, cu2, cv2)
    im = distLF(:,:,:,cu1,cv1);
    distLF(:,:,:,cu2,cv2) = im;
end

function distLF = errorMisalignment_1(LF)
      % copy LF
    distLF = LF;
    distLF = copy(distLF, 1, 3, 6, 9);
    distLF = copy(distLF, 13, 9, 11, 5);
    distLF = copy(distLF, 6, 8, 15, 3);
end

function distLF = errorMisalignment_2(LF)
      % copy LF
    distLF = LF;
    distLF = copy(distLF, 5, 14, 7, 5);
    distLF = copy(distLF, 1, 10, 14, 3);
    distLF = copy(distLF, 9, 14, 10, 9);
    
    distLF = copy(distLF, 5, 9, 14, 3);
    distLF = copy(distLF, 13, 11, 4, 11);
    distLF = copy(distLF, 8, 10, 6, 5);
    
    distLF = copy(distLF, 9, 15, 2, 6);
    distLF = copy(distLF, 1, 9, 8, 3);
    distLF = copy(distLF, 3, 7, 1, 15);          
end


