function LF_Image = RenderMicro(LF)
    [S,T,c,U,V] = size(LF);

    LF2=permute(LF,[5 4 3 1 2]);
    % tile microimages into one big 2D image
    LF_Image = zeros(S*U,T*V,c);

    for s=0:S-1
        for t=0:T-1
            im=LF2(:,:,:,s+1,t+1);
            LF_Image(s*U+1:s*U+U,t*V+1:t*V+V,1:3)=im;
        end
    end
    LF_Image=LF_Image/255;
end