function LF2 = lfadjust(LF1, max)
%LFADJUST adjust lightfield randomly.
%   LF2 = LFADJUST(LF1, MAX) randomly adjusts the contrast of the 
%   perspective images with a maximum scale of MAX (between 0 and 1) in 
%   lightfield LF1 and stores the result in lightfield LF2.

if(ndims(LF1) == 5)
    [~,~,~,u,v] = size(LF1);
elseif(ndims(LF1) == 4)
    [~,~,u,v] = size(LF1);
else
    error('Lightfields must have 4 or 5 dimensions');
end

% due to indecisiveness
contrasts = max*rand(u,v);
contrasts = cat(3, contrasts, 1 - contrasts);

LF1 = im2double(LF1);
LF2 = zeros(size(LF1));
for i = 1:u
    for j = 1:v
        LF2(:,:,:,i,j) = imadjust(LF1(:,:,:,i,j), [contrasts(i,j,1), contrasts(i,j,2)]);
    end
end

