 LFS={};	
 //"lego":["lego_blur1","lego_blur_2"] ,
 // "lion":["lego_blur1","lego_blur_2"] ,
 // "angle":["lego_blur1","lego_blur_2"] ,
 //};
DISTORTIONS=["blur_1","blur_2","blur_4","missing_1of10","missing_1of5","missing_1of2","compression_1","compression_2","compression_4","noise_1","noise_2","noise_4"];
IMAGES=["lego","angle","cave","lion"];
currentOriginal=null;
for(i in IMAGES){
	distortions={};
	x=0;
	for(d in DISTORTIONS){
		distortions[d]=IMAGES[i]+"_"+DISTORTIONS[d];
	}
	LFS[IMAGES[i]]=distortions;
}

function show(frame,lightfield) {
	link="aperture_local.swf?lightfield=lightfields/"+lightfield+".zip";
	$('#'+frame).attr('src',link);
	//document.getElementById(frame).setAttribute('src', link);
}

function showDistortion(lightfield) {	
	i=lightfield.indexOf("_");
	if(i>-1){
		original=lightfield.substr(0,i);
		if(original!=currentOriginal){
			show("f1",original);
			currentOriginal=original;
		}
		show("f2",lightfield);
	}
}

function append(frame,parent,lightfield){
		   $('<input/>')
        .attr('type', 'radio')
		.attr('value', lightfield)
        .attr('rolerole', 'menuitem')
		.attr('name', 'originals')
		.click(function(){showDistortion(lightfield)})
        .appendTo(parent);
		parent.append(lightfield);
		$('</br>').appendTo(parent);
}

$(document).ready ( function(){
   originalsParent=$('#selection_original');
   distortionsParent=$('#selection_distortion');

   for(originalLF in LFS){
	   append('f1',originalsParent,originalLF);
	   distortions=LFS[originalLF];
	   for(distLF in distortions){
			append('f2',distortionsParent,distortions[distLF]);
	   }
   }
});

