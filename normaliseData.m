function userRating = normaliseData()
    csv=csvread('userStudy.csv');
    maxOfUser=max(csv,[],2);
    minOfUser=min(csv,[],2);

    normData = (csv-repmat(minOfUser,1,18))./double(repmat(maxOfUser-minOfUser,1,18));

    normAvgRating = mean(normData,1);

    avgMin=mean(minOfUser);
    avgMax=mean(maxOfUser);

    userRating = normAvgRating*(avgMax-avgMin)+avgMin; 
    userRating=userRating/10;
end