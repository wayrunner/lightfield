function [depthSsimValue,depthImage] = CustomDepthSSIM(LF1,LF2)
    mindepth = -3;
    maxdepth = 3;
    %depth resolution
    depthslices = 30;
    %calc for center persp.
    du=0; 
    dv=0; 

    %calc depth, might take several minutes!!!
    depth = calcDepth(LF1,mindepth,maxdepth,depthslices,du,dv);
    %optional filtering
    depth = medfilt2(depth,[3 3]);

    %calc depth, might take several minutes!!!
    dDepth = calcDepth(LF2,mindepth,maxdepth,depthslices,du,dv);
    %optional filtering
    dDepth = medfilt2(dDepth,[3 3]);

    Target = dDepth;
    Source = depth;
    [depthSsimValue, depthImage] = ssim(Target,Source);
end