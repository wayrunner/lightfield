function value=customSSIM(LF1,LF2)
    % Implement your custom ssim method here    
    value=0;
    [renderSSIM, ~]=CustomRenderSSIM(LF1,LF2,1,1,-4,0);
    value=value+renderSSIM;
    [renderSSIM, ~]=CustomRenderSSIM(LF1,LF2,1,1,-4,4);
    value=value+renderSSIM;
    [renderSSIM, ~]=CustomRenderSSIM(LF1,LF2,1,1,0,0);
    value=value+renderSSIM;
    [renderSSIM, ~]=CustomRenderSSIM(LF1,LF2,1,1,4,0);
    value=value+renderSSIM;
    [renderSSIM, ~]=CustomRenderSSIM(LF1,LF2,1,2,4,4);
    value=value+renderSSIM;
    [~,~,~,cSSIM] = CustomMicroSSIM(LF1,LF2);
    value=value+cSSIM;
    value=value/6;
end