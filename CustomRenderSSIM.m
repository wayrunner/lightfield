function [ssimVal, ssimImage] = CustomRenderSSIM(LF1,LF2,d,a,u,v)
    r1 = RenderLF(LF1,d,a,u,v); 
    r2 = RenderLF(LF2,d,a,u,v); 
    [ssimVal, ssimImage] = ssim(r1,r2);
end