function [meanSSIM,varSSIM,covSSIM,ssimVal] = CustomMicroSSIM(LF1,LF2)
    [S,T,c,U,V] = size(LF1);

    LF1_=permute(LF1,[5 4 3 1 2]);
    LF2_=permute(LF2,[5 4 3 1 2]);

    % calculate gray scale image
    L1=0.2126*LF1_(:,:,1,:,:)+0.7152*LF1_(:,:,2,:,:)+0.0722*LF1_(:,:,3,:,:);
    L2=0.2126*LF2_(:,:,1,:,:)+0.7152*LF2_(:,:,2,:,:)+0.0722*LF2_(:,:,3,:,:);

    % tile microimages into one big 2D image
    
    diffMean = zeros(S,T,c);
    diffVar = zeros(S,T,c);
    diffCov = zeros(S,T,c);


    C1=0.01;
    C2=0.01;
    C3=0.01;

    for s=1:S
        for t=1:T
            im1=L1(:,:,1,s,t);
            im1=double(im1(:));
            im2=L2(:,:,1,s,t);
            im2=double(im2(:));
            m1=mean(im1);
            m2=mean(im2);
            diffMean(s,t,1:3)=(2*m1*m2+C1)/(m1*m1+m2*m2+C1);%abs(m1-m2);%
            co=cov(im1,im2);
            sd1=sqrt(co(1));
            sd2=sqrt(co(4));
            diffVar(s,t,1:3)=(2*sd1*sd2+C2)/(sd1*sd1+sd2*sd2+C2);%abs(v1-v2);%v2/255;%
            diffCov(s,t,1:3)=(co(2)+C1)/(sd1*sd2+C3);
        end
    end
    ssimImage=diffCov.*diffVar.*diffMean;

    %diffMean=diffMean/255;
    %diffVar=diffVar/255;
    meanSSIM = mean(diffMean(:));
    varSSIM = mean(diffVar(:));
    covSSIM = mean(diffCov(:));
    ssimVal = mean(ssimImage(:));
    
%     figure;
%     subplot(2,3,1),imshow(RenderMicro(LF1));
%     subplot(2,3,2),imshow(RenderMicro(LF2));
%     subplot(2,3,3),imshow(diffMean);
%     title(strcat('Mean SSIM=',num2str(meanSSIM)));
%     subplot(2,3,4),imshow(diffVar);
%     title(strcat('SD SSIM=',num2str(varSSIM)));
%     subplot(2,3,5),imshow(diffCov);
%     title(strcat('Co Var SSIM=',num2str(covSSIM)));
%     subplot(2,3,6),imshow(ssim);
%     title(strcat('SSIM=',num2str(ssim)));
    
end