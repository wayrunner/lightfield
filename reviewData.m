function userRating = reviewData(d1,d2,t)
    c=corrcoef(d1,d2);
    figure;
    scatter(d1,d2);
    title(strcat(t,' cor=',num2str(c(1,2))));
end