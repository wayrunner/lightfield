% Load possible distortions for the lightfields folder and the defined
% quality
dist=CreateDistortions('C:temp/lightfields/',0.5);

results=(1:18)*0;

len=dist.len();
ids=deal(dist.getIds());

userRatings=normaliseData();
for i=1:len
    id=ids(i);
    disp(strcat('Working on: ',id));
    
    ret=dist.getDistortedLF(id{1});
    [LF,dLF,userRating]=deal(ret{:});

    % Save the distorted lightfield to the disc
    dist.save(id{1},dLF);
    
    result = customSSIM(LF,dLF);
    fprintf('The 2D - SSIM value is %0.4f.\n',result);
    %disp(strcat('Custom SSIM: ',result));

end

predictions=zeros(18,2);
predictions(:,1)=userRatings;
predictions(:,2)=results;
csvwrite('predictions.csv',predictions);
reviewData(userRatings,results);    
    