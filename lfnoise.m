function LF2 = lfnoise(LF, type)
%LFNOISE applies image noise to the camera array (in some way). 
%Improvements desirable
%   LF2 = LFNOISE(LF, 'salt & pepper') returns a distorted lightfield LF2
%   of the lightfield LF as if some cameras were broken.
%   LF2 = LFNOISE(LF, 'gaussian') returns a distorted lightfield LF2 where
%   different cameras have different intensities 
%   (NOTE: due to bad implementation only reduced intensities!).

if(ndims(LF) == 5)
    [s,t,c,u,v] = size(LF);
elseif(ndims(LF) == 4)
    [s,t,u,v] = size(LF);
else 
    error('lightfield needs to have either 4 or 5 dimensions');
end

%TODO: far from perfect
DIST = imnoise(ones(u,v), type);
if(ndims(LF) == 5)
    perm = [4,5,3,1,2];
    MASK = repmat(DIST, 1, 1, c, s, t);
elseif(ndims(LF) == 4)
    perm = [3,4,1,2];
    MASK = repmat(DIST, 1, 1, s, t);
end

MASK = permute(MASK, perm);
LF2 = im2uint8(im2double(LF) .* MASK);

end

