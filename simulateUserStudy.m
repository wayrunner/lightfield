% Load possible distortions for the lightfields folder and the defined
% quality
quality = 0.5;
dist=CreateDistortions('C:temp/lightfields/',quality);
%%
%dist.prepareOriginal('lego_blur_1','lego');
%dist.prepareOriginal('cave_blur_1','cave');
%dist.prepareOriginal('lion_blur_1','lion');
%dist.prepareOriginal('angle_blur_1','angle');
%dist.prepareOriginal('beans_blur_1','beans');

studyContent = '';

%min and max disparity factor for light field have to be found manually
%(these values work for the full resolution lego knight light field)
mindepth = -3;
maxdepth = 3;
%depth resolution
depthslices = 30;
%calc for center persp.
du=0; 
dv=0; 

fromCu = 0.4;
toCu = 2.5;
cuSteps = 5;

fromCv = 0.4;
toCv = 2.5;
cvSteps = 5;

fromDV = 0.1;
toDV = 0.7;
DVSteps = 5;

ids=deal(dist.getIds());
id=ids(1);
ret=dist.getDistortedLF(id{1});
[LF,dLF,userRating]=deal(ret{:});
% size of lightfield (dimension order as it is being loaded: S,T,c,U,V
[T,S,c,U,V] = size(LF);

fromAP = 2;
toAP = V/2-0.2;
apertureSteps = 4;

% Build title
studyContent=strcat('Name');
studyContent=strcat(studyContent,';');

for disparityfactor = fromDV:(toDV-fromDV)/DVSteps:toDV
    for apertureradius = fromAP:(toAP-fromAP)/apertureSteps:toAP
        for cu = fromCu:(toCu-fromCu)/cuSteps:toCu
            for cv = fromCv:(toCv-fromCv)/cuSteps:toCv

                studyContent=strcat(studyContent,' ap: ');
                studyContent=strcat(studyContent,num2str(apertureradius,2));
                studyContent=strcat(studyContent,' dv: ');
                studyContent=strcat(studyContent,num2str(disparityfactor,2));
                studyContent=strcat(studyContent,' cu: ');
                studyContent=strcat(studyContent,num2str(cu,2));
                studyContent=strcat(studyContent,' cv: ');
                studyContent=strcat(studyContent,num2str(cv,2));
                studyContent=strcat(studyContent,';');

            end
        end
    end
end

studyContent=strcat(studyContent,';');
studyContent=strcat(studyContent,'Overall');

studyContent=strcat(studyContent,';');
studyContent=strcat(studyContent,'Depth');

studyContent=strcat(studyContent,';');
studyContent=strcat(studyContent,'Micro');

studyContent=strcat(studyContent,';');
studyContent=strcat(studyContent,'Mean');

studyContent=strcat(studyContent,';');
studyContent=strcat(studyContent,'Var');

studyContent=strcat(studyContent,';');
studyContent=strcat(studyContent,'Cov');

studyContent=strcat(studyContent,';');
studyContent=strcat(studyContent,'Ssim');

studyContent=strcat(studyContent,'\n');

ids=deal(dist.getIds());
for id=ids
    disp(strcat('Working on: ',id));
    
    % Save the distorted lightfield to the disc
    % dist.save(id{1},dLF);

    rateCount = 0;
    rateSum = 0;

    studyContent=strcat(studyContent,id{1});
    studyContent=strcat(studyContent,';');
   
    ret=dist.getDistortedLF(id{1});
    [LF,dLF,userRating]=deal(ret{:});
    % size of lightfield (dimension order as it is being loaded: S,T,c,U,V
    [T,S,c,U,V] = size(LF);

    for disparityfactor = fromDV:(toDV-fromDV)/DVSteps:toDV
        for apertureradius = fromAP:(toAP-fromAP)/apertureSteps:toAP
            for cu = fromCu:(toCu-fromCu)/cuSteps:toCu
                for cv = fromCv:(toCv-fromCv)/cuSteps:toCv

                    originRenderedLightField = RenderLF(LF,disparityfactor,round(apertureradius),cu,cv); 
                    modifiedRenderedLightField = RenderLF(dLF,disparityfactor,round(apertureradius),cu,cv); 

                    %figure;
                    %subplot(1,2,1),imshow(originRenderedLightField);
                    %subplot(1,2,2),imshow(modifiedRenderedLightField);

                    Target = modifiedRenderedLightField;
                    Source = originRenderedLightField;
                    [ssimval, ssimmap] = ssim(Target,Source);
                    rateSum = rateSum+ ssimval;

                    fprintf('The 2D - SSIM value is %0.4f.\n',ssimval);

                    studyContent=strcat(studyContent,num2str(ssimval,3));
                    studyContent=strcat(studyContent,';');

                    rateCount = rateCount+ 1;
                end
            end
        end 
    end

    overalSSIM = rateSum / rateCount;
    fprintf('The total SSIM value is %0.4f.\n',overalSSIM);
    
    %calc depth, might take several minutes!!!
    depth = calcDepth(LF,mindepth,maxdepth,depthslices,du,dv);
    %optional filtering
    depth = medfilt2(depth,[3 3]);

    %calc depth, might take several minutes!!!
    dDepth = calcDepth(dLF,mindepth,maxdepth,depthslices,du,dv);
    %optional filtering
    dDepth = medfilt2(dDepth,[3 3]);

    Target = dDepth;
    Source = depth;
    [depthSSIMval, ssimmap] = ssim(Target,Source);
    fprintf('The Depth - SSIM value is %0.4f.\n',depthSSIMval);
    
    %switch to direction order: V,U,c,S,T (LF2(:,:,:,s,t) gives you one proper microimage at s,t
    %(keep in mind that matlab stores images in column-order)
    LF2=permute(LF,[5 4 3 1 2]);
    % tile microimages into one big 2D image
    LF2_Image = zeros(S*U,T*V,c);

    for s=0:S-1
        for t=0:T-1
            im=LF2(:,:,:,s+1,t+1);
            LF2_Image(s*U+1:s*U+U,t*V+1:t*V+V,1:3)=im;
        end
    end
    LF2_Image=LF2_Image/255;

    
    %switch to direction order: V,U,c,S,T (LF2(:,:,:,s,t) gives you one proper microimage at s,t
    %(keep in mind that matlab stores images in column-order)
    DLF2=permute(dLF,[5 4 3 1 2]);
    % tile microimages into one big 2D image
    DLF2_Image = zeros(S*U,T*V,c);

    for s=0:S-1
        for t=0:T-1
            im=DLF2(:,:,:,s+1,t+1);
            DLF2_Image(s*U+1:s*U+U,t*V+1:t*V+V,1:3)=im;
        end
    end
    DLF2_Image=DLF2_Image/255;
    
    Target = DLF2_Image;
    Source = LF2_Image;
    [microSSIMval, ssimmap] = ssim(Target,Source);
    fprintf('The Micro - SSIM value is %0.4f.\n',microSSIMval);
    
    [meanSSIM,varSSIM,covSSIM,ssimVal] = CustomMicroSSIM(LF,dLF);

    studyContent=strcat(studyContent,';');
    studyContent=strcat(studyContent,num2str(overalSSIM,3));

    studyContent=strcat(studyContent,';');
    studyContent=strcat(studyContent,num2str(depthSSIMval,3));

    studyContent=strcat(studyContent,';');
    studyContent=strcat(studyContent,num2str(microSSIMval,3));
    
    studyContent=strcat(studyContent,';');
    studyContent=strcat(studyContent,num2str(meanSSIM,3));
    
    studyContent=strcat(studyContent,';');
    studyContent=strcat(studyContent,num2str(varSSIM,3));
    
    studyContent=strcat(studyContent,';');
    studyContent=strcat(studyContent,num2str(covSSIM,3));
    
    studyContent=strcat(studyContent,';');
    studyContent=strcat(studyContent,num2str(ssimVal,3));
    
    studyContent=strcat(studyContent,'\n');

    fileName = 'ssim_study.csv';
    fileID = fopen(fileName,'w+');
    fprintf(fileID,studyContent);
    fclose(fileID);    

end

fileName = 'ssim_study.csv';
fileID = fopen(fileName,'w+');
fprintf(fileID,studyContent);
fclose(fileID);
