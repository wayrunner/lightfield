function PrepareForStanford(path,firstimpos,zipName)
% IMPORTLF Saves a lightfield to the defined location (pathTo) and uses the
% filenames from pathFrom    
    xmlContent = '';
    xmlContent=strcat(xmlContent,'<lightfield>\n');
    
    % list light field perspectives
    filesFrom = dir(strcat(path,'/*.png'));
    U=sqrt(size(filesFrom,1));
    V=U;
    for i=1:size(filesFrom,1)
        % compute current u and v coordinate
        cv = floor((i-1)/U)+1;
        cu = mod((i-1),U)+1;
        
        % invert if first image is not top left image
        if(firstimpos(1) == 1)
            cu = U-cu+1;
        end
        if(firstimpos(2) == 1)
            cv = V-cv+1;    
        end
        
        xmlContent=strcat(xmlContent,'<subaperture src="');
        xmlContent=strcat(xmlContent,filesFrom(i).name);
        xmlContent=strcat(xmlContent,'" u="');
        xmlContent=strcat(xmlContent,num2str(cu));
        xmlContent=strcat(xmlContent,'" v="');
        xmlContent=strcat(xmlContent,num2str(cv));
        xmlContent=strcat(xmlContent,'" />\n');
    end
    
    % Write XML File
    xmlContent=strcat(xmlContent,'</lightfield>');
    xmlName = strcat(path,'/','lightfield.xml');
    fileID = fopen(xmlName,'w');
    fprintf(fileID,xmlContent);
    fclose(fileID);
    fclose('all');
    
    % Create Zip archive
    targetFiles = dir(strcat(path,'/*.*'));
    targetFilesList = {targetFiles.name}';
    
    if exist(zipName, 'file')==2
        delete(zipName);
    end
    
    winrar = 1;
    if(winrar == 1)
        pngCommand = strcat('winrar a -afzip "',zipName,'" -ep1 "',path,'*"');
        [status,results] = dos(pngCommand);
    elseif(winrar == 0)
        zip(zipName,{'*.xml','*.png'},path);
    end
end

