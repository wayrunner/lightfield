function LF_Image = RenderPerspective(LF)
    [T,S,c,U,V] = size(LF);
    LF_Image = zeros(U*S,V*T,c);
    for u=0:U-1
        for v=0:V-1
            im=LF(:,:,:,u+1,v+1);
            LF_Image(u*S+1:u*S+S,v*T+1:v*T+T,1:3)=im;
        end
    end
    LF_Image=LF_Image/255;
end