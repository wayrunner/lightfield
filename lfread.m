function A = lfread( path, fmt, scale, u, v )
%LFREAD Read lightfield from graphics files in folder.
%   A = LFREAD(PATH, FMT) reads a square grayscale or color lightfield from 
%   the files in the directory specified by the string PATH. The files in 
%   the directory are read in descending alfabetical order (assumption that 
%   holds for the provided lightfields). If the path is not in the current 
%   directory, or in a directory on the MATLAB path, specify the full 
%   pathname. 
%   
%   The text string FMT specifies the format of the file by its standard
%   file extension. For example, specify 'gif' for Graphics Interchange 
%   Format files. To see a list of supported formats, with their file 
%   extensions, use the IMFORMATS function.
%
%   The return value A is an array containing the lightfield data. If the 
%   file contains a grayscale image, A is an S-by-T-by-U-by-V array. If the 
%   file contains a truecolor image, A is an S-by-T-by-3-by-U-by-V array. 
%   For TIFF files containing color images that use the CMYK color space, 
%   A is an M-by-N-by-4-by-U-by-V array. See TIFF in the Format-Specific 
%   Information section for more information.
%
%   A = LFREAD(PATH, FMT, SCALE) reads a square lightfield from the files 
%   in the directory specified by PATH with format FMT. Additionally, the 
%   image is scaled with factor SCALE to limit memory usage.
%
%   A = LFREAD(PATH, FMT, SCALE, U) reads a lightfield from the files in 
%   the directory specified by PATH with format FMT and rescaled with 
%   factor SCALE. Parameter U allows to define the number of horizontal
%   camera positions. This might be useful for non-square lightfields.
%
%   A = LFREAD(FILENAME, FMT, SCALE, U, V) constructs a lightfield from a 
%   single file specified by FILENAME with format FMT and rescaled with 
%   factor SCALE with U horizontal and V vertical directions.
%
%   See also: IMREAD

if(~isdir(path))
    im = imresize(imread(path, fmt), scale);
    A = repmat(im, [1,1,1,u,v]);
    return;
end

filenames = dir(strcat(path, '/*.', fmt));
filenames = sortrows({filenames.name}', -1);
nr = length(filenames);

switch(nargin)
    case 2
        scale = 1;
        u = floor(sqrt(nr));
        v = u;
    case 3
        u = floor(sqrt(nr));
        v = u;
    case 4
        v = floor(nr / u);
end

im = imresize(imread(fullfile(path, filenames{1})), scale);
A = zeros([size(im), u, v], 'uint8');

A(:,:,:,1) = im;
for i = 2:nr
    A(:,:,:,i) = imresize(imread(fullfile(path, filenames{i})), scale);
end

A = squeeze(A);

end